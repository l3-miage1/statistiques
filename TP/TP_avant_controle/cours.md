## Methode des moments
On veut estimer un parametre
* Etape1 -> On fait appel au moment theorique do'ordre 1 : l'esperance
* Etape2 -> On écrit le parametre en fonction du moment théorique
* Etape3 -> Plug-in : on remplace moment théorique par moment empirique( moyenne empirique) pour former l'estimateur $\hat{\theta}_n$ de $\theta$

Exemple1 -> $$X_1, X_2, ..., X_n avec n=independante+in bernoulli(p)$,
loi de Bernoulli de parametre p inconue

* Etape1 -> on fait appel à l'esperance : $E(X) = p$
* Etape2 -> p= $E(X)$
* Etape3 -> $\hat{p}_n = \bar{X}$

Exemple2 -> $X_1, X_2, ..., X_n n U(0, \theta) avec -\theta-inconnu.$
* Etape1 -> on fait appel à l'esperance : $E(X) = \frac{\theta}{2}$ 
* Etape2 -> $\theta = 2E(X)$
* Etape3 -> $\hat{\theta}_n = 2\bar{X}_n$

Exemple3 -> $X_1, X_2, ..., X_n n Geo(p)$ avec p inconnu.
* Etape1 -> on fait appel à l'esperance : $E(X) = \frac{1-{p}}{p}$
* Etape2 -> $p = \frac{1}{1+E(X)}$
* Etape3 -> $\hat{p}_n = \frac{1}{1+\bar{X}_n}$