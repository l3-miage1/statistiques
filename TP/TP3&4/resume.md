# R

%%
setwd("/home/o22102866@campus.univ-orleans.fr/Travail/statistiques/TP/TP3")
setwd("/home/baboux/COURS/MIAGE/2023-2024/lab/statistiques/TP/TP3")
getwd()
%%


> Nous allons distinguer dans ce qui suit le cas où l'on observe réellement les valeurs prises par notre variable (données brutes) de celui où l'on observe seulement la classe à laquelle elles appartiennent (données classées).

# Données quantitative

## A. Lorsque l'on dispose des données brutes

## EXERCICE (INTRODUCTION):
![[TP_Stat.Desc_QUANT.pdf#page=4]]
### 1. Lancer R et générer 150 données provenant d'une loi N(0,1).

```R
ech = rnorm(150, 0, 1)
```
### 2. Tracer la distribution avec un diagramme en bâtons.
```R
freq = table(ech)/length(ech)
plot(freq)
```
### 3. Qu'observe-t-on? Commenter.
> diagramme en bâton pas optimal pour variables continue car aucune variable n'est égal a une autre.
> Pour ce genre d'échantillon faire un diagramme en bâton n'a aucun sens car aucune variable n'est égale à une autre.

### 4. Quel est le mode de la variable.
> On ne peut pas parler de mode

# Travail Perso
## Exercice 1
1. Créer un vecteur correspondant à ces données. 
```R
nbEnfants = c(0:6)
effectifEnfants = c(43, 58, 49, 25, 13, 8, 4)
enfantDansMenage = c(rep(nbEnfants, effectifEnfants))
```

2. Représenter la distribution par un diagramme en bâtons et déterminer la valeur du mode. Utiliser range pour retrouver les modalités maximales et minimales. 
```R
t1 = table(enfantDansMenage)
plot(t1)
min_max = range(t1)
min = min_max[1]
max = min_max[2]
```

3. Donner le tableau des fréquences et des fréquences cumulées. Tracer la courbe des fréquences cumulées. 
```R
freq = table(enfantDansMenage)/length(enfantDansMenage)
plot(freq)
freq_cum = cumsum(freq)
plot(nbEnfants, freq_cum, type='s')
abline(h=0.25, col='red')
abline(h=0.50, col='blue')
abline(h=0.75, col='green')
```

4. En déduire la valeur des quartiles. Utiliser la commande boxplot.stats pour contrôler vos résultats 
```R
q1 = quantile(enfantDansMenage, 0.25)
q2 = quantile(enfantDansMenage, 0.50)
q3 = quantile(enfantDansMenage, 0.75)
boxplot.stats(t1)
```

5. Déterminer la moyenne, la variance, et l'écart-type (à la main puis avec R)
```R
mean(t1) # moyenne
var(t1) # variance
sd(t1) # ecart type
sqrt(var(t1)) # ecart type
```


---
## Exercice 2

1. Créer un vecteur correspondant à ces données.
```R
Age = c(19:23)
Effectif = c(1, 3, 9, 5, 2)
AgeParPersonne = rep(Age, Effectif)
```
2. Déterminer le mode, la médiane et la moyenne de ces données (à la main puis avec R).
```R
median = quantile(AgeParPersonne, 0.50) # 21
moyenne = mean(AgeParPersonne) # 21.2
```
3. Comment ces valeurs changent-elles si un étudiant de 45 ans vient rejoindre la classe? Commenter
```R
AgeParPersonne = c(AgeParPersonne, 45)
median = quantile(AgeParPersonne, 0.50) # 21
moyenne = mean(AgeParPersonne) # 22.3
```
On peut en conclure que les valeur qui s'écarte beaucoup des valeurs de base on une grande influence sur la moyenne mais pas sur la médian.

---
## **Exercice 3**
### On étudie la fréquentation d'un guichet dans une grande banque au centre de Paris dans la but d'améliorer ce service. On fait une étude sur 2000 périodes de 2 minutes choisies sur la tranche horaire de 11h à 12h et on compte le nombre de personnes x_i entrant dans l'agence pendant la période i donnée. Les données recueillies se trouvent dans la librairie lycee dans le fichier guichet.

1. Charger ces données sur R. Créer un vecteur file contenant la colonne « queue » du fichier guichet. Déterminer la moyenne, le mode et la médiane de ces données.
```R
setwd('D:/BOULOT/Licence MIAGE 2023/statistiques/TP/TP3&4')
load("lycee/data/Rdata/guichet.rda")
head(guichet)
queue = guichet$queue
mean(queue) # 2.495
median(queue) # 2
mode(queue) # "numeric"
```
2. Tracer la boîte à moustache associée à ces données, et déterminer les valeurs des quartiles, de la variance et de l'écart-type.
```R
boxplot(queue) # boîte à moustache
quantile(queue, 0.25) # Q1 = 1
quantile(queue, 0.50) # Q2 = 2
quantile(queue, 0.75) # Q3 = 3
var(queue) # variance = 2.476213
sd(queue) # ecart type = 1.573599
```
3. Construire le diagramme en bâton correspondant à la distribution des données. Représenter également la courbe des fréquences cumulées.
```R
freq = table(queue)/length(queue)
plot(freq)
fcum = cumsum(freq)
plot(fcum,  type='s')
```
4. Proposer une modélisation de la distribution du caractère étudié. Représenter sur un même graphique la distribution empirique et la modélisation.
```R

```

---
## EXERCICE 4: 
Une compagnie d'assurance a effectué une étude concernant le nombre d'accidents déclarés en 1998 par ses souscripteurs de police d'assurance « tous risques ». Le résultat est donné dans le tableau ci- dessous:

Nombre k d'accident déclarés | 0 | 1 | 2 | 3 
:- | -- | -- | -- | -:
Effectifs | 14698 | 1466 | 74 | 3 

1. Créer un vecteur correspondant à ces données 
2. Quelle est la population étudiée, la taille de l'échantillon, le caractère considéré, la nature de ce caractère? 
3. Par quels graphiques peut-on représenter ces données. Faites-en deux. 
4. Déterminer la moyenne empirique, la variance empirique, la médiane, le premier et le troisième quartile de ces données. 
5. Par quel modèle probabiliste peut-on envisager de modéliser le nombre d'accidents?