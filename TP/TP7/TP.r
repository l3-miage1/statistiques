#  Estimation paramètrique
# trajet domicile fac
# Qui est la population : ensemble des étudiants de l'UTT
# Variables Etudiées : temps de trajet, sexe, mode de transport,
# distance domicile fac
#  Nature des variables :
#  Variables quantitatives : temps de trajet
#  Variables qualitatives : sexe, mode de transport, distance domicile fac
#  Variables discrètes : sexe, mode de transport
#  Variables continues : temps de trajet, distance domicile fac
#  Représenter la distribution des obserations
# au travers d'un graphique approprié

centres <- seq(5, 55, 10)
centres
effectifs <- c(32, 30, 10, 6, 1, 1)
data <- rep(centres, effectifs)
# répète les centres autant de fois que les effectifs
bornes <- seq(0, 60, 10)
hist(data, breaks = bornes, freq = FALSE)
# ABSOLUMENT mettre le break pour avoir les bonnes bornes
# freq=FALSE pour avoir la densité
# freq=TRUE pour avoir les effectifs

# Moyenne et variance proche + qqchose d'autre= loi de poisson
#  Loi normale courbe en cloche theoreme de gauss
# Dans ce cas ci Loi exponentielle

# Intervale de confiance (IC) à 95% pour la moyenne
# pour un paramètre d'ésperance lambda = E[X]
#  On recherche un IC de niveau (1-x)

## EXO P.15 LAIT
# Soit X la variable aléatoire donnant la production laitiere annuelle des vaches
# X suit une loi quelconque d'éspérance E[X] inconnue et de variance v² = 1000²
n <- 35
xbar <- 4790
# On veut IC de niveau 99% pour E[X]
alpha <- 0.01 # 1-0.99
sigma <- 1000 # écart type
quant <- qnorm(1 - (alpha / 2))
borne_inf <- xbar - (quant * (sigma / sqrt(n)))
borne_sup <- xbar + (quant * (sigma / sqrt(n)))
borne_inf
borne_sup

# Que peut on faire si l"écart type est inconnu ?
# On remplace sigma par s
# On remplace qnorm par qt
# On remplace n par n-1
# Sous R
# Cas2: echantillon de taille n < 30
# Seul cas resolvable: echantillon gaussien
# X1, X2, ..., Xn ~ N(m, sigma²)
# Cas2.1 : sigma² connu
# [ Xbar - qnorm(1-alpha/2) * sigma / sqrt(n) ;
# Xbar + qnorm(1-alpha/2) * sigma / sqrt(n) ]
# Cas 2.2 : sigma² inconnu, on l'estime par s²n-1 MAIS on
# remplace le quantile de la loi normale par celui de la loi de student
# à (n-1) degrés de liberté

## EXO durée de vie fin de P.15
# Soit X la variable aléatoire donnant la durée de vie d'un composant
# électronique X suit une loi quelconque d'éspérance E[X] 
# inconnue et de variance inconnu et n petit = 3
ech <- c(45, 48, 49.5)
xbar <- mean(ech)
sn <- sd(ech)
n <- length(ech)
quant <- qt(0.975, 2)
borne_inf <- xbar - (quant * (sn / sqrt(n)))
borne_sup <- xbar + (quant * (sn / sqrt(n)))
borne_inf
borne_sup