## setwd("/home/o22102866@campus.univ-orleans.fr/Téléchargements")
## setwd("/home/baboux/COURS/MIAGE/2023-2024/lab/statistiques/TP/TP2")
## getwd()


class = read.table("Classe.data.txt", header=TRUE) # pour qu'il ne considère pas l'entete comme une donnée
head(class)

## EXO 1
## individu = etudiant ; taille = 150
## Type de variable
## Numero : qual, nomiale
## Age : quant, discret
## taille : quant, ?nominal
## departement : qual, nomiale
## difficulte : qual, ordinale
## serie : qual, nominale

## EXO 2
sexe = class$sexe
table(sexe)
freq = table(sexe)/length(sexe)

## graphics
plot(freq)
barplot(freq)
pie(freq) # camember

sort(freq)
rev(sort(freq))
names(rev(sort(freq)))
names(rev(sort(freq)))[1]
fc = cumsum(freq)
fc

difficulte=class$difficulte
freq=table(difficulte)/length(difficulte)
cumsum(freq)

boxplot(freq) # boite a moustache


boxplot(difficulte)
difficS=class$difficulte[class$serie=="S"] # Difficulte pour filiere S
difficES=class$difficulte[class$serie=="ES"] # Difficulte pour filiere S
difficS
par(mfrow=c(1,2)) # separt l'affichage plot en 2
boxplot(difficS)
boxplot(difficES)

quantile(difficulte, 0.25)
median(difficulte)
quantile(difficulte, c(0.25, 0.5, 0.75))
quantile(difficulte, 0.1)


rep(1, 50)
rep(c(1, 0.5, 5), c(4,3,10))
