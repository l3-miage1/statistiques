Revision Stat

## Intervale de confiance
X±Z(s/sqrt(n)​) // c'est xbar le X

où:
    Xˉ est la moyenne de l'échantillon,
    Z est la valeur critique associée au niveau de confiance (pour un niveau de confiance de 99%, ZZ est environ 2.576),
    S est l'écart type de l'échantillon,
    n est la taille de l'échantillon.