## Exercice 1

# 2
nombreErreurs = c(0:5)
nombreAppreils = c(103, 140, 92, 42, 18, 5)
data = rep(nombreErreurs, nombreAppreils)

# 4
par(mfrow=c(1,2))
pie(table(data))
barplot(table(data))

# 5
par(mfrow=c(1,1))
mean(data)
var(data)

# 7
freq = table(data)/length(data)
plot(freq)
lines(0:5,dbinom(0:5,400,0.7),col='red')


## Exercice 2
# 3
Classe = c(60, 20, 40, 20, 60)
Effectif = c(112, 176, 461, 157, 94)
freq = Effectif/sum(Effectif)
barplot(freq/Classe, names.arg=Classe) # pic = classe modale

# 4
moyenneDuree= c(90, 130, 160, 190, 230)
data = rep(moyenneDuree, Effectif)
mean(data)
var(data)

# 5
fcum = cumsum(freq)

# 6
quantile(data, 0.25)
median(data)
quantile(data, 0.75)


## Exercice 3
# 2
cout = c(48, 43, 77, 89, 50, 40, 56, 62, 100, 47, 71, 58, 102, 35)
age = c(15, 8, 36, 41, 16, 8, 21, 21, 53, 10, 32, 17, 58, 6)
plot(age, cout)

# 3 
cor(age, cout)

# 4
reg = lm(cout~age)
reg
abline(reg)

# 5
plot(age, cout)
abline(reg, col='red')

# 6
summary(reg)
