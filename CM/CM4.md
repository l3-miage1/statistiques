### [Aide MD math](https://learninglab.gitlabpages.inria.fr/mooc-rr/mooc-rr-ressources/module1/ressources/introduction_to_markdown_fr.html)
covariance si X et Y s'éloigne de la moyenne ensemble

SI X et Y sont indépendant, Avec E -> espérance <br>
    cov(X, Y) = E(X-E(X)) * E(Y-R(Y))

cov(X, Y) = 1/5 Σ (X<sub>i</sub>-X<sub>n</sub>)


- E {\displaystyle {\mathcal {E}}}

- cor(X, Y) = cov(X, Y)/S<sub>x</sub>*S<sub>y</sub>
    - ${S_x}$ = $\sqrt{S^2_{n, x}}$
    - ${S_y}$ = $\sqrt{S^2_{n, y}}$
- cor(X, Y) = cor(Y, X)
- cor (X, X) = 1
- p(X, Y) $\in$ [-1, 1
- SI |p(X, Y)| = 1 alors il existe une droite tq ${Y_i}$ = a ${X_i}$ + b