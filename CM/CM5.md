
### [Aide MD math](https://learninglab.gitlabpages.inria.fr/mooc-rr/mooc-rr-ressources/module1/ressources/introduction_to_markdown_fr.html)
# Régression linéaire (sur une variable)
*  Y variable à expliquer (en variable cible/target)
* X variable explicative (ou variable prédictive/ prédictor)
Dans notre étude, on veut déterminer si Y dépend de X linéairement (=droite)
On cherche la meilleure relation entre X et Y afin de pouvoir prédire une nouvelle variable de Y en fonction de X.

On voudrais trouver a et b tq :
	Y =aX +b + ε
avec ε l'erreur la plus petite possible

On veut minimiser :
	$Y_{1}$ = $aX_1 + ε_1$
	j'ai pas réussi
%% On créer une droite et on reporte tout les point sur cette droite puis on regarde la somme des erreurs -> plus l'erreur est petite plus la droite est représentative. %%
On considère les sommes des erreurs faites en considérant les points sur la droite (ax+b) au lieu des points observer
%% erreur juste sur l'ordonnée donc on ne mesure que l'ordonnée'%%
S(a,b) = $\sum_{i=1}^{n}$ $(g_i(ax_i + b))²$  = $\sum_{i=1}^{n}$ $ε²_i$ 
On veut minimiser cette somme S(a,b) en les variables a et b.
Développement : 
	S(a,b) = $\sum_{i=1}^{n}$ $y²_i - 2 y_i (ax_i + b)+(ax_i + b)²$
	           = $\sum_{i=1}^{n}$ (cherche un peu la pas possible d’être un assister comme ça)
Dérivée en a :
	S(a, b) = $\sum_{i=1}^{n}$ $(-2 y_i x_i + 2 ax²_i + 2b x_i)$
				= $\sum_{i=1}^{n}$ $(-2y_i+2ax_i+2b)$
S(a,b) = 0 -> $a\sum_{i}x²_i$  = $\sum_{i=1}x_iy_i-b\sum_{i=1}x_i$ 
S(a,b) = 0 -> $a\sum_{i}x_i$  = $\sum_{i=1}y_i-nb$ 
on a
	b= ta mere c insuivable

