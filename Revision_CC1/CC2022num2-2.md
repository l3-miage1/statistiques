# Controle L3 Miage 2021-2022
## Exercice 1
1) Donner la population étudiée, la taille de l’échantillon, le caractère (variable) considéré et la nature de ce caractère.
```R
## La population étudier est les boites d'oeuf
## Taille de l'échantillon=500
## Le caractère étudier est le nombre d'oeuf non conformes
## nature = Variable Discrete quantitative
```
2) Créer sous R un vecteur data correspondant à ces données.
```R
nbOeufConforme = c(0:3)
nbBoite = c(310, 150, 36, 4)
data = rep(nbOeufConforme, nbBoite)
```
3) Par quels graphiques peut on représenter ces données ? Faites en deux sous R, dans la même fenêtre graphique.
```R
par(mfrow=c(2,1))
table = table(data)
barplot(table)
plot(table)
pie(table)
```
#### Pour afficher les ligne sur le graph  
> abline(h=0.25, col='red') <br> 
abline(h=0.50, col='blue') <br>
abline(h=0.75, col='green')
4) Calculer la moyenne, la variance et les quartiles de ces données.
```R
mean(data)
var(data)
q1 = quantile(data, 0.25)
q2 = quantile(data, 0.50)
q3 = quantile(data, 0.75)
```
5) Par quelle loi classique peut-on envisager de modéliser la distribution de la variable ?
> On peut imaginer représenter cette distribution a l'aide d'un Lois binomial, car les oeufs de sont soit non conforme soit conforme. Et sont ensuite ranger dans des boites que l'on étudit. <br> Un loi binomial de taille 500, de probabilité X
6) Représenter sur un même graphique la distribution empirique et la modélisation proposée. Commenter les résultats obtenus.
```R
plot(table/sum(table))
x <- 0:500
curve(dbinom(x, 500, 0.1), add=TRUE, col='red')

```

---

## Exercice 2

Classe | [0;0.5[ | [0.5;1.5[ | [1.5;3[ | [3;6[ | [6;8[ | [8;10[ |[10;12[
:-|-|-|-|-|-|-|-:
Effectif (Nb de pépites)| 20 | 28 | 30 | 16 | 4 | 1 | 1

## nature = Variable Continu qualitative
1. Préciser la population considérée, la variable étudiée et sa nature, la taille de l’échantillon.
```R
## Les pépites d'or
## Leur taille en mm
## Variable continue qualitative
```
2. Représenter la distribution des observations au travers d’un graphique approprié et déterminer la classe modale.
```R
Classe = c(0.5, 1, 1.5, 3, 2, 2, 2)
effectifPepite = c(20, 28, 30, 16, 4, 1, 1)
freq = effectifPepite/sum(effectifPepite)
round(freq, 2)
barplot(freq/Classe, names.arg=Classe) # pic = classe modale
```
3. Expliquer pourquoi on pense, au vu de la forme de l’histogramme, modéliser le diamètre des pépites par une loi exponentielle et tracer le graphique correspondant.
```R
hist(freq/Classe)
curve(dexp(x, 1/mean(freq/Classe)), add=TRUE, col='red')
```
4. Calculer la moyenne, la variance et l’écart-type associés à ces données.
```R
moyenneClasse = c(0.25, 1, 2.25, 4.5, 7, 9, 11)
data=c(rep(moyenneClasse, effectifPepite))
mean(data)
var(data)
sd(data)
```

5. Déterminer les valeurs des quartiles.
```R
q1 = quantile(data, 0.25)
q2 = quantile(data, 0.50)
q3 = quantile(data, 0.75)
```
---


## Exercice 3
> On considère le fichier ’pressionarterielle’ dans la librairie ’Lycée’. Il détermine le taux de cholesterol et le fait d’être ou non fumeur pour 2000 individus. Le nom du fichier étant très long il est conseillé de le renommer

1. Décrire la population et sa taille, la variable étudiée et sa nature.
```R
## La population est les individus
## La population étudier est les 2000 individus
## La variable étudier est le taux de cholesterol
## La nature de la variable est continue quantitative pour le cholesterol et discrete qualitative pour le fait d'être fumeur ou non
```

2. Représenter par des boxplots la répartition du taux de cholesterol en fonction de fumeur/non fumeur. Comparer les quartiles. Pour chaque sous-population (fumeur ou non-fumeur) déterminer la moyenne, et la variance.
```R
setwd("/D:/BOULOT/Licence MIAGE 2023/statistiques/TP/TP3&4")
load("statistiques/TP/TP3&4/lycee/data/Rdata/pressionarterielle.rda") # chargement du fichier
# renamer les lignes du dataframe
names(pressionarterielle) = c("cholesterol", "fumeur")
boxplot(pressionarterielle$cholesterol ~ pressionarterielle$fumeur)
```

3. Faire un histogramme en 20 classes homogènes du taux pour la population fumeuse, puis pour la population non fumeuse. Déterminer les classes modales. Essayer de trouver une modélisation pour chaque sous-population et tracer la densité de la loi proposée.
```R
hist(pressionarterielle$cholesterol[pressionarterielle$fumeur=="oui"], breaks=20)
# La classe modale est se situe entre 180 et 200
# Loi de poisson
# Loi de Poisson
x <- 0:300  # Remplacez 20 par la valeur maximale appropriée
lambda <- mean(pressionarterielle$cholesterol[pressionarterielle$fumeur=="oui"])
y <- dpois(x, lambda)

# Créer un graphique
plot(x, y, type = "h", lwd = 2, col = 'blue', xlab = "X", ylab = "Probabilité", main = "Loi de Poisson")
lines(x, dpois(x, lambda), col = 'red')


hist(pressionarterielle$cholesterol[pressionarterielle$fumeur=="non"], breaks=20)
#curve(dnorm(x, mean(pressionarterielle$cholesterol[pressionarterielle$fumeur=="non"]), sd(pressionarterielle$cholesterol[pressionarterielle$fumeur=="non"])), add=TRUE, col='red')
```
4. Faire un histogramme du taux en 20 classes homogènes pour la population totale. Qu’en déduisez-vous?
```R
hist(pressionarterielle$cholesterol, breaks=20)
## On peut voir que la population est composé de 2 sous population
## On peut en déduire que si la personne fume elle est plus susceptible d'avoir un taux de cholesterol élevé
```
