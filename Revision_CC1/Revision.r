getwd() #donne ta position dans l'arborcésence

#charge un fichier rda
load("/home/o22208670@campus.univ-orleans.fr/Documents/Statistiques/TP3/lycee_1.0/lycee/data/Rdata/guichet.rda") 

#Sois guichet la variable créer par la lecture du fichier rda

guichet


#Sois file la partie de la variable guichet sur la queue
file =guichet$queue

#donne un resumé du vecteur 
summary()
head(guichet)

#créer un vecteur de valeur [2,3,4]
c(2,3,4)

#la boite a moustache du vecteur file
boxplot(file)

#la variance du vecteur file 
var(file)

#écart-type du vecteur file
sd(file)

#créer une table a partir du vecteur file -> de la forme effectif - valeur 
table(file)

#créer un diagramme de frequence du vecteur file (Freq sois table sur len)
plot(table(file) /length(file), ylab = "freq",main = "fréquence pour l'affluence")


#créer un vecteur avec 510 fois 1 puis 248 fois 2 etc
Z = c(rep(1,510), rep(2,248), rep(3,132) , rep(4,55), rep(5,28), rep(6,15), rep(7,2), rep(8,8),10)

#créer un vecteur de effectif du dessus 
n = c(510,248,132,55,29,15,2,8,0,1)

#créer un vecteur des valeurs de dessus
x0 = 1:10

#affichage de effectif avec les valeurs , les effectifs , type , labely , labelx
plot(x0 , n , type='h', ylab='effectif' , xlab='nb enfants' )

#returns the value of the Poisson probability density function (ou 0->9 est le valeur de X et le lambda est l'E sois la moyenne)
dpois(0:9,mean(queue))

#returns the value of Binomial probability density function ( vecteur de lavaleur prise par X , )
pbinom(q, size, prob, lower.tail = FALSE) 

#returns line of the value of Geometrique probability function () sur l'intervalle 1:10
lines(1:10,dgeom(1:10,1/(mean(X)+1),log=FALSE),col='blue')

lines(x0,dexp(x0,1/mean(X),log=FALSE),col='red')





#sequence de 1 a 10 avec un pas de 0.1
x0=seq(1,10,by=0.01)


simuler un échantillon de taille 500, de loi N(0,1)
X=rnorm(500,0,1)

#Vecteur classe = vecteur de la longueur de chaque intervalle
Classe=c(2000,2000,5000,5000,10000,14000) 
Eff=c(50,190,210,40,4,2)
